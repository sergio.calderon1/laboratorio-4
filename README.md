# Laboratorio 5
Sé que esto no va acá pero no existe Word en Linux :D

Recordatorio para README: no usar hashtag a comienzo de linea porque lo confunde con titulos


8. Para agregar un campo a una tabla de modelo ya creada:

	En consola

	rails generate migration agregar_campo_a_nombreModeloPlural


	Agregamos esta linea MANUALMENTE al método change de la migracion creada

	add_column :nombreModeloPlural, :nombreCampo, :tipoCampo
	
	En consola, corremos la migración

	rake db:migrate

	
11. Para agregar un controlador con su respectiva vista:

	En consola

	rails generate controller nombreControlador_nombreMetodo
	
	
	Agregamos esta linea dentro de un elemento de la vista principal (menú)

	<%= link_to "Texto a mostrar", nombreControlador_nombreMetodo_path %>
