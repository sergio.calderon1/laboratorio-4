require "test_helper"

class MainControllerTest < ActionDispatch::IntegrationTest
  test "should get under_construction" do
    get main_under_construction_url
    assert_response :success
  end
end
