Rails.application.routes.draw do
  get 'main/under_construction'
  get 'victimas/index'
  
  resources :tweets
  resources :monstruos
  
  root 'tweets#default'
end
