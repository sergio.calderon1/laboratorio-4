class MonstruosController < ApplicationController
  
  def index
  end

  def show
    @monstruo = Monstruo.find(params[:id])
    if (@monstruo.tweets.count < 1)
      redirect_to monstruos_path, notice: "El monstruo no tiene tweets creados"
    end
  end

  def edit    
  end
  
  def destroy
    @eliminar = Monstruo.find(params[:id])

    if @eliminar.destroy
      redirect_to monstruos_path, notice: "El monstruo ha sido eliminado con éxito"
    else
      flash[:error] = "Hubo un error al eliminar el monstruo"
    end
  end
end
