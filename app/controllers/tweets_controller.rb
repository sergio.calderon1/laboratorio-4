# encoding = utf-8
class TweetsController < ApplicationController
  
  def new
  	@tweet = Tweet.new
  end

  def default    
  end
  
  def index
  end

  def show
  end

  # -----------------
  # CREATE
  # El método create recibe params = { tweet: { monstruo_id: valor, estado: valor } }
  def create
    @tweet = Tweet.new(params.require(:tweet).permit(:monstruo_id, :estado))

    if @tweet.save
      redirect_to @tweet, notice: "Tweet creado con éxito"
    else
      flash[:error] = "Hubo un error al crear el tweet"
      render :new
    end
  end
end